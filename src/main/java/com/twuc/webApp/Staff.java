package com.twuc.webApp;

import javax.persistence.*;

@Entity
public class Staff {

    @Id
    private Integer id;

    @Embedded
    private Name name;


    public Integer getId() {
        return id;
    }

    public Name getName() {
        return name;
    }

    public Staff(Integer id, Name name) {
        this.id = id;
        this.name = name;
    }

    public Staff() {
    }
}
