package com.twuc.webApp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tw_office")
public class Office {

    @Id
    private Long id;

    public Office() {
    }

    @Column(nullable = false,length = 36)
    private String city;

    public Long getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public Office(Long id, String city) {
        this.id = id;
        this.city = city;
    }

}
