package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class OfficeJpaTest {

    @Autowired
    private OfficeRepository repo;

    @Autowired
    private EntityManager em;

    @Test
    void hello_world() {
        assertTrue(true);
    }

    @Test
    void should_save_an_entity() {
        Office savedOffice = repo.save(new Office(1L, "wuhan"));
        repo.flush();
        assertNotNull(savedOffice);
    }

    @Test
    void should_persist_entity_with_em() {
        em.persist(new Office(2L,"xian"));
        em.flush();
        em.clear();
        Office office = em.find(Office.class, 2L);

        assertEquals(Long.valueOf(2),office.getId());
        assertEquals("xian",office.getCity());
    }

    @Test
    void should_exception_when_city_is_null_with_em() {
        assertThrows(PersistenceException.class,()->{
            em.persist(new Office(3L,null));
            em.flush();
            em.clear();
        });
    }

    @Test
    void should_exception_when_city_is_null_with_repo() {
        assertThrows(DataIntegrityViolationException.class,()->{
            repo.save(new Office(4L,null));
            repo.flush();
        });
    }

    @Test
    void should_less_than_36bytes_with_city_repo() {
        assertThrows(DataIntegrityViolationException.class,()->{
            repo.save(new Office(5L,"shdgfksdhufgishuifsiudhfsiuhdfjsdhgfiusdhfiuwhfiushdiufhasjdfghweiufgh"));
            repo.flush();
        });
    }

    @Test
    void should_less_than_36bytes_with_city_em() {
        assertThrows(PersistenceException.class,()->{
            em.persist(new Office(6L,"shdgfksdhufgishuifsiudhfsiuhdfjsdhgfiusdhfiuwhfiushdiufhasjdfghweiufgh"));
            em.flush();
        });
    }
}
