package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest(showSql =false)
@DirtiesContext(methodMode = DirtiesContext.MethodMode.BEFORE_METHOD)
public class StaffTest {

    @Autowired
    private EntityManager em;

    @Test
    void should_save_staff() {
        em.persist(new Staff(1,new Name("x","z")));
        em.flush();
        em.clear();
        Staff staff = em.find(Staff.class, 1);
        assertEquals(Integer.valueOf(1),staff.getId());
        assertEquals("x",staff.getName().getFirstName());
        assertEquals("z",staff.getName().getLastName());
    }
}
